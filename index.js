const express = require('express');
const fileUpload = require('express-fileupload');
const bodyParser = require('body-parser');
const app = express();
const path = require('path');
const amqp = require('amqplib/callback_api');
const router = express.Router();
const port = process.env.port || 3000

app.use(bodyParser.json());         // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
})); 

app.use(fileUpload({
    limits: { fileSize: 50 * 1024 * 1024 },
}));

router.get('/',function(req, res){
    res.sendFile(path.join(__dirname+'/index.html'));
});

router.get('/status',function(req, res){
    res.sendFile(path.join(__dirname+'/status.html'));
});

router.post('/upload',function(req, resp){
    console.log(req.body)
    amqp.connect('amqp://0806444524:0806444524@152.118.148.95:5672/%2f0806444524', function(error0, connection) {
        if (error0) {
            throw error0;
        }
        connection.createChannel(function(error1, channel) {
            if (error1) {
                throw error1;
            }
            var exchange = '1606879230_DIRECT';
            var routingKey = "SagabRoutingKey";
            channel.assertExchange(exchange, 'direct', {
                durable: false
            });
            var msg = JSON.stringify(req.body)
            channel.publish(exchange, routingKey, Buffer.from(msg));
            console.log(" [x] Sent %s: '%s'", routingKey, msg);
            setTimeout(function() {
                connection.close();
            },  500);
        });
    });
});

app.use('/', router);
app.listen(port);

console.log(`Example app listening on port ${port}!`);
